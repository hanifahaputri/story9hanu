from django.db import models

class book(models.Model):
    lst_id = models.CharField(max_length=200, primary_key=True)
    image = models.TextField(null=True)
    title = models.CharField(max_length=200, null=True)
    authors = models.CharField(max_length=200, null=True)
    publisher = models.CharField(max_length=200, null=True)
    publishedDate = models.CharField(max_length=200, null=True)
    likes = models.IntegerField(default=0)

    def __str__(self):
        return self.title
