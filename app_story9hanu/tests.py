from django.test import TestCase, Client
from django.urls import resolve
from .views import home
from .models import book
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class Story9UnitTest(TestCase):
    def test_story9_is_exist(self):
      response = Client().get('/')
      self.assertEqual(response.status_code, 200)

    def test_story9_404_error(self):
      response = Client().get('/page/')
      self.assertEqual(response.status_code, 404)

    def test_url_fires_correct_views_method(self):
      response = resolve('/')
      self.assertEqual(response.func, home)

    def test_story9_render_html(self):
      response = self.client.get('/')
      self.assertTemplateUsed(response, 'home.html')

    def test_new_image(self):
      new_image = book.objects.create(image='This is an image')

      all_available_image = book.objects.all().count()
      self.assertEqual(all_available_image, 1)
    
    def test_new_title(self):
      new_title = book.objects.create(title='Title Sample')

      all_available_title = book.objects.all().count()
      self.assertEqual(all_available_title, 1)

    def test_new_id(self):
      new_id = book.objects.create(lst_id=1)

      all_available_id = book.objects.all().count()
      self.assertEqual(all_available_id, 1)

    def test_new_authors(self):
      new_author = book.objects.create(authors=1)

      all_available_author = book.objects.all().count()
      self.assertEqual(all_available_author, 1)

    def test_new_publisher(self):
      new_publisher = book.objects.create(publisher=1)

      all_available_publisher = book.objects.all().count()
      self.assertEqual(all_available_publisher, 1)

    def test_new_publishedDate(self):
      new_publishedDate = book.objects.create(publishedDate=1)

      all_available_publishedDate = book.objects.all().count()
      self.assertEqual(all_available_publishedDate, 1)
    
    def test_new_likes(self):
      new_likes = book.objects.create(likes=1)

      all_available_likes = book.objects.all().count()
      self.assertEqual(all_available_likes, 1)

# Gak ada functional kak, karena di local bisa tapi pas di deploy fail terus

# class Story9FunctionalTest(TestCase):
#     def setUp(self):
#       chrome_options = Options()
#       chrome_options.add_argument('--dns-prefetch-disable')
#       chrome_options.add_argument('--no-sandbox')        
#       chrome_options.add_argument('--headless')
#       chrome_options.add_argument('disable-gpu')
#       self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#       super(Story9FunctionalTest, self).setUp()
        
#     def tearDown(self):
#       self.selenium.quit()
#       super(Story9FunctionalTest, self).tearDown()

#     def test_search(self):
#       selenium = self.selenium
#       selenium.get('http://127.0.0.1:8000/')

#       time.sleep(10)

#       search = selenium.find_element_by_id("search-bar")
#       input_text = "B"

#       search.send_keys(input_text)
#       time.sleep(10)

#       page = selenium.page_source
#       self.assertIn(input_text, page)

#     def test_like(self):
#       selenium = self.selenium
#       selenium.get('http://127.0.0.1:8000/')
#       response_content = selenium.page_source

#       search = selenium.find_element_by_id("search-bar")
#       input_text = "B"

#       search.send_keys(input_text)

#       time.sleep(10)

#       like = selenium.find_element_by_xpath(u"//button[contains(text(), 'like')]")
#       like.click()

#       time.sleep(10)

#       page = selenium.page_source
#       self.assertIn("like", page)

#     def test_unlike(self):
#       selenium = self.selenium
#       selenium.get('http://127.0.0.1:8000/')
#       response_content = selenium.page_source

#       search = selenium.find_element_by_id("search-bar")
#       input_text = "B"

#       search.send_keys(input_text)

#       time.sleep(10)

#       unlike = selenium.find_element_by_xpath(u"//button[contains(text(), 'unlike')]")
#       unlike.click()

#       time.sleep(10)

#       page = selenium.page_source
#       self.assertIn("unlike", page)
