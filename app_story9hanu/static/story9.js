var lst = [];
$(document).ready(function() {
    $("#search-bar").on("keyup", function(e) {
        var q = e.currentTarget.value.toLowerCase()
        var xhttprequest = new XMLHttpRequest();
        console.log(q);
        xhttprequest.onreadystatechange = function() {
            if(this.readyState == 4 && this.status == 200 ) {
                var data = JSON.parse(this.responseText);
                lst = data.items;
                $('#search-result').html('')
                var result = '';
                for (var i = 0; i < data.items.length; i++) {
                        result += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                        "<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td>" +
                        "<td class='align-middle'>" + 
                        "<button type='button' id='like' onclick='like("+ i +")'> like </button>" +
                        "<button type='button' id='unlike' onclick='unlike("+ i +")'> unlike </button>" +
                        "<p id='like"+ i + "' value='0'> 0 </p>" + "</td>" +
                        "</tr>"
                    }
                $('#search-result').append(result);
                }
                else {
                }
            }
        xhttprequest.open("GET", "https://www.googleapis.com/books/v1/volumes?q=" + q, true);
        xhttprequest.send();
    });
});
function like(i){
    var data = JSON.stringify(lst[i]);
    var xhttprequest = new XMLHttpRequest();
    xhttprequest.onreadystatechange= function() {
        if (this.readyState == 4 && this.status == 200) {
            var like = JSON.parse(this.responseText).likes;
            $("#like" + i).html(like);
        }
    }
        xhttprequest.open("POST","like");
        xhttprequest.send(data);
}

function unlike(i) {
    var data = JSON.stringify(lst[i]);
    var xhttprequest = new XMLHttpRequest();
    xhttprequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var unlike = JSON.parse(this.responseText).unlike;
            $("#like" + i).html(unlike);
        }
    }
        xhttprequest.open("POST", "unlike");
        xhttprequest.send(data);
}

function favorites(){
    var xhttprequest = new XMLHttpRequest();
    xhttprequest.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var top = JSON.parse(this.responseText).favorites_list;
            $('.modal-content').html('');
            for (var i = 0; i < 5; i++){
                $('.modal-content').append(
                    "<p>"+ top[i]['title'] + "</p>" 
                )
            }
        }
    }
    xhttprequest.open("GET", "favorites", true)
    xhttprequest.send();
}