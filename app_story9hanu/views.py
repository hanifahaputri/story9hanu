from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from .models import book
import json

def home(request):    
    return render(request, 'home.html')

@csrf_exempt
def like(request):
    data = json.loads(request.body)
    try:
        book_data = book.objects.get(lst_id = data['id'])
        book_data.likes += 1
        book_data.save()

    except:
        book_data = book.objects.create(
            lst_id = data['id'],
            image = data['volumeInfo']['imageLinks']['smallThumbnail'],
            title = data['volumeInfo']['title'],
            authors = data['volumeInfo']['authors'],
            publisher = data['volumeInfo']['publisher'],
            publishedDate = data['volumeInfo']['publishedDate'],
            likes = 1
            )
    return JsonResponse({'likes': book_data.likes})

@csrf_exempt
def unlike(request):
    data = json.loads(request.body)
    print(data)
    try:
        book_data = book.objects.get(lst_id = data['id'])
        if book_data.likes > 0:
            book_data.likes -=1
        book_data.save()

    except:
        book_data = book.objects.create(
            lst_id = data['id'],
            image = data['volumeInfo']['imageLinks']['smallThumbnail'],
            title = data['volumeInfo']['title'],
            authors = data['volumeInfo']['authors'],
            publisher = data['volumeInfo']['publisher'],
            publishedDate = data['volumeInfo']['publishedDate'],
            )
    return JsonResponse({'unlike': book_data.likes})

def favorites(self):
    favorites_list = []
    favorites = book.objects.order_by('-likes')
    for books in favorites:
        favorites_list.append({'title': books.title})
        
    return JsonResponse({'favorites_list': favorites_list})
